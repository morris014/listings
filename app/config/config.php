<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));


$env = 'default';
if(@$_SERVER['HTTP_HOST'] == 'localhost')
{
    $env = 'localhost';
    //DEFINE RABBIT MQ CONFIG
    define('RABBIT_HOST',$env);
    define('RABBIT_PORT',5672);
    define('RABBIT_USER', 'guest');
    define('RABBIT_PASS', 'guest');
}
else
{
    $env = 'devops';
    //test
    define('RABBIT_HOST','192.168.122.161');
    define('RABBIT_PORT',5672);
    define('RABBIT_USER', 'gene');
    define('RABBIT_PASS', 'gwapings1234');
    //$http_origin = $_SERVER['HTTP_ORIGIN'];
    //header("Access-Control-Allow-Origin: $http_origin");
}   


$database['localhost']['adapter'] = 'Mysql';
$database['localhost']['host'] = 'localhost';
$database['localhost']['username'] = 'root';
$database['localhost']['password'] = '';
$database['localhost']['dbname'] = 'big_data';

$database['devops']['adapter'] = 'Mysql';
$database['devops']['host'] = '192.168.122.115';
//$database['devops']['host'] = 'localhost';
$database['devops']['username'] = 'project';
$database['devops']['password'] = '123';
$database['devops']['dbname'] = 'big_data';

return new \Phalcon\Config(array(
    'database' => $database[$env],
    'application' => array(
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'pluginsDir'     => APP_PATH . '/app/plugins/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'baseUri'        => '/big_data/',
    )
));

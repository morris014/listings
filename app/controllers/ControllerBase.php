<?php

use Phalcon\Mvc\Controller;
use Phalcon\Image\Adapter\GD as PhImageGD;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Assets\Manager as Assets;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

class ControllerBase extends Controller
{
	public function _paginate($data,$page,$limit){	
		$paginator =  new PaginatorModel(
				array(
					"data"=>$data,
					"limit"=>$limit,
					"page"=>$page
				)
		);
		$page = $paginator->getPaginate();
		return $page->items;
	}
}


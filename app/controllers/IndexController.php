<?php

class IndexController extends ControllerBase
{

    public function indexAction()
    {	

    	$started_at = microtime(true);
        $search = $this->request->getPost('search');
        $search_text = (!$search) ? '' : $search;
        $data = UserInfoTb::listAll($search_text);
        $page = ($this->request->getPost('page_no') !== null) ? $this->request->getPost('page_no') : 1;
        $limit =1000;
        $user_info =$this->_paginate($data,$page,$limit);

        $total_item = sizeof($data);
        $total_time = microtime(true) - $started_at;
        $total_page = $total_item / $limit;

        $this->view->setVar('user_info',$user_info);
    	$this->view->setVar('page_content','index/index');
        $this->view->setVar('search_text',$search_text);
        $this->view->setVar('total_results',$total_item);
        $this->view->setVar('total_time',$total_time);
     	$this->view->setVar('total_page',$total_page);
        $this->view->setVar('page_no',$page);
    }

}


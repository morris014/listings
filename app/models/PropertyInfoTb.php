<?php

class PropertyInfoTb extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $personal_prop_id;

    /**
     *
     * @var string
     */
    public $notes;

    /**
     *
     * @var string
     */
    public $personal_notes;

    /**
     *
     * @var string
     */
    public $highlights;

    /**
     *
     * @var integer
     */
    public $commercial;

    /**
     *
     * @var string
     */
    public $unit;

    /**
     *
     * @var string
     */
    public $building_name;

    /**
     *
     * @var string
     */
    public $street_number;

    /**
     *
     * @var string
     */
    public $street;

    /**
     *
     * @var string
     */
    public $user_id;

    /**
     *
     * @var integer
     */
    public $prop_type_id;

    /**
     *
     * @var integer
     */
    public $list_type_id;

    /**
     *
     * @var integer
     */
    public $country_id;

    /**
     *
     * @var integer
     */
    public $region_id;

    /**
     *
     * @var integer
     */
    public $province_id;

    /**
     *
     * @var integer
     */
    public $barangay_id;

    /**
     *
     * @var integer
     */
    public $city_id;

    /**
     *
     * @var string
     */
    public $postal;

    /**
     *
     * @var integer
     */
    public $assoc_id;

    /**
     *
     * @var string
     */
    public $cover_image;

    /**
     *
     * @var string
     */
    public $video_url;

    /**
     *
     * @var string
     */
    public $authority_file;

    /**
     *
     * @var integer
     */
    public $authority_duration;

    /**
     *
     * @var double
     */
    public $price;

    /**
     *
     * @var double
     */
    public $price_to;

    /**
     *
     * @var integer
     */
    public $disp_price;

    /**
     *
     * @var integer
     */
    public $nego_price;

    /**
     *
     * @var string
     */
    public $inclusive_assoc;

    /**
     *
     * @var string
     */
    public $condition;

    /**
     *
     * @var string
     */
    public $furniture;

    /**
     *
     * @var string
     */
    public $improvements;

    /**
     *
     * @var integer
     */
    public $minimum_lease;

    /**
     *
     * @var string
     */
    public $total_views;

    /**
     *
     * @var string
     */
    public $pos_lat;

    /**
     *
     * @var string
     */
    public $pos_lng;

    /**
     *
     * @var integer
     */
    public $status_flag;

    /**
     *
     * @var integer
     */
    public $property_status;

    /**
     *
     * @var integer
     */
    public $cobroke_flag;

    /**
     *
     * @var integer
     */
    public $hide_street_flag;

    /**
     *
     * @var string
     */
    public $indoor_others;

    /**
     *
     * @var string
     */
    public $outdoor_others;

    /**
     *
     * @var string
     */
    public $date_available;

    /**
     *
     * @var string
     */
    public $date_posted;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_updated;

    /**
     *
     * @var string
     */
    public $date_expiration;

    /**
     *
     * @var string
     */
    public $google_address;

    /**
     *
     * @var string
     */
    public $street_address;

    /**
     *
     * @var integer
     */
    public $quality_score;

    /**
     *
     * @var integer
     */
    public $temp_created;

    /**
     *
     * @var integer
     */
    public $temp_id;

    /**
     *
     * @var string
     */
    public $License_to_sell;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'property_info_tb';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PropertyInfoTb[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PropertyInfoTb
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

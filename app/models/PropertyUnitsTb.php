<?php

class PropertyUnitsTb extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $units;

    /**
     *
     * @var string
     */
    public $property_id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $notes;

    /**
     *
     * @var integer
     */
    public $bedrooms;

    /**
     *
     * @var integer
     */
    public $bathrooms;

    /**
     *
     * @var integer
     */
    public $parking;

    /**
     *
     * @var double
     */
    public $land_area;

    /**
     *
     * @var string
     */
    public $land_area_unit;

    /**
     *
     * @var double
     */
    public $floor_area;

    /**
     *
     * @var string
     */
    public $floor_area_unit;

    /**
     *
     * @var integer
     */
    public $status_flag;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_updated;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'property_units_tb';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PropertyUnitsTb[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PropertyUnitsTb
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

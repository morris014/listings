<?php

use Phalcon\Mvc\Model\Validator\Email as Email;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class UserInfoTb extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $fname;

    /**
     *
     * @var string
     */
    public $mname;

    /**
     *
     * @var string
     */
    public $lname;

    /**
     *
     * @var string
     */
    public $nname;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var integer
     */
    public $show_notes;

    /**
     *
     * @var string
     */
    public $notes;

    /**
     *
     * @var string
     */
    public $headline;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var integer
     */
    public $type_id;

    /**
     *
     * @var string
     */
    public $birthdate;

    /**
     *
     * @var string
     */
    public $gender;

    /**
     *
     * @var string
     */
    public $marital;

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var string
     */
    public $token;

    /**
     *
     * @var string
     */
    public $image_url;

    /**
     *
     * @var string
     */
    public $thumb_url;

    /**
     *
     * @var string
     */
    public $fb_user_id;

    /**
     *
     * @var string
     */
    public $li_profile_id;

    /**
     *
     * @var string
     */
    public $google_id;

    /**
     *
     * @var integer
     */
    public $sendpepper_id;

    /**
     *
     * @var integer
     */
    public $status_flag;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_updated;

    /**
     *
     * @var string
     */
    public $google_plus;

    /**
     *
     * @var string
     */
    public $twitter_creator;

    /**
     *
     * @var string
     */
    public $facebook_account;

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );

        if ($this->validationHasFailed() == true) {
            return false;
        }

        return true;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_info_tb';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserInfoTb[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserInfoTb
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public  function listAll($searchText ="")
    {
        $conditions = '';
        $limit = '';
        $binds = array();
        $additional = "";
        $order_by = "";
        $groupby = "";
        if($searchText != ""){
            $conditions .= " 
                            WHERE 
                            MATCH(put.units) AGAINST('".$searchText."') OR
                            MATCH(rttt.name) AGAINST('".$searchText."') OR
                            MATCH(rtt.name) AGAINST('".$searchText."') OR
                            MATCH(rct.name) AGAINST('".$searchText."') OR
                            MATCH(rbt.name) AGAINST('".$searchText."') OR
                            MATCH(rpt.name) AGAINST('".$searchText."') AND pit.id is not null
                            ";
            $additional = ", MATCH(put.units) AGAINST('".$searchText."') + 
                             MATCH(rttt.name) AGAINST('".$searchText."') + 
                             MATCH(rtt.name) AGAINST('".$searchText."') + 
                             MATCH(rct.name) AGAINST('".$searchText."') +
                             MATCH(rbt.name) AGAINST('".$searchText."') +  
                             MATCH(rpt.name) AGAINST('".$searchText."') as relevance  ";
            $order_by = " ORDER BY relevance DESC ";
            $groupby = " GROUP BY pit.id ";
        }else{
            $conditions = " WHERE pit.id is not null or pit.id > 0";
        }


        // A raw SQL statement
        $phql   = "SELECT put.land_area,put.land_area_unit,put.floor_area,put.floor_area_unit,CONCAT(IF(length(uit.fname),uit.fname,' '),' ',IF(length(uit.lname),uit.lname,' ')) as fullname,
                          CONCAT(IF(length(pit.unit),pit.unit,' '),' ',IF(length(rbt.name),rbt.name,' '),' ',
                                    IF(length(pit.street),pit.street,' '),' ',
                                    IF(length(rct.name),rct.name,' '),' ',
                                    IF(length(rpt.name),rpt.name,' ')) as address,
                          uit.email,pit.price,put.bedrooms,put.bathrooms,put.parking,
                          umt.mobile as mobile, umt.home_phone as phone,
                          rttt.name as list_type,uit.id as user_id,
                          pit.id as property_id,rtt.name as property_type,
                          rct.name as city,rpt.name as province,
                          rbt.name as barangay,pit.id".$additional." 
                  FROM UserInfoTb uit
                  LEFT JOIN PropertyInfoTb pit ON pit.user_id = uit.id
                  LEFT JOIN RefProvincesTb rpt ON pit.province_id = rpt.id
                  LEFT JOIN RefCitiesTb rct ON rct.id = pit.city_id
                  LEFT JOIN RefBarangaysTb rbt ON rbt.id = pit.barangay_id
                  LEFT JOIN RefTypesTb rtt ON pit.prop_type_id = rtt.id
                  LEFT JOIN RefTypesTb rttt ON pit.list_type_id = rttt.id
                  LEFT JOIN UserMetaTb umt ON umt.user_id = uit.id
                  LEFT JOIN PropertyUnitsTb put ON put.property_id = pit.id
                ".$conditions.$groupby.$order_by.$limit;
        
 //  var_dump($phql); die;
        $data = $this->modelsManager->executeQuery($phql);
        return $data;
    }

}
